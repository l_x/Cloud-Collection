package cn.lx.cloud;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class CloudApplication {
    OkHttpClient client = new OkHttpClient();
    public String getUUid(){

        String url = "https://login.wx.qq.com/jslogin?appid=wx782c26e4c19acffb&fun=new&lang=zh_CN";
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();

        Call call = client.newCall(request);

        try {
            Response response = call.execute();

            String str = response.body().string();

            str = str.replace("window.QRLogin.code = 200; window.QRLogin.uuid = \"", "")
                    .replace("\";", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        SpringApplication.run(CloudApplication.class, args);
    }

}
